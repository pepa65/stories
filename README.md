# Stories เรื่องราว
**ฟังเรื่องตามลำดับไตยวนจากพระคริสตธรรมคัมภีร์**
**Audio Northern Thai Bible stories**

## App
The `app` directory has all the files to serve the Stories app with allows
to pick one of the 42 stories and play the audio while watching the image.

## Videos
The `vid` directory has all the "videos" (consisting of image + audio)
that are on youtube.com/channel/UCR1tYjwYAvjbvTmd9vE6F-Q

## Conversions
* Images: PNG image data, 672 x 478, 8-bit/color RGB, non-interlaced
* For Grayscale PNG: convert IMAGE -alpha off -colorspace Gray IMAGE.png
* Audio: ffmpeg -i IN.mp3 -b:a 64k -map a OUT.mp3
* Video: ffmpeg -i IN.png -i IN.mp3 OUT.mp4
* All in `app` directory, except the video which is in the `vid` directory.

