self.addEventListener('fetch', (event) => {
	console.log('Fetching:', event.request.url);
	event.respondWith(fetch(event.request));
});
